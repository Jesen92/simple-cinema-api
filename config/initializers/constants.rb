# Load constants as OpenStruct for ease of access
class Constants
  class << self
    (Dir.entries("#{Rails.root}/config/constants") - %w[. ..]).each do |filename|
      constant_name = filename.split('.').first
      define_method :"#{constant_name}" do
        memoize_results("@#{constant_name}") do
          JSON.parse(YAML.safe_load(File.open("#{Rails.root}/config/constants/#{filename}", 'r')).to_json, 
                     object_class: OpenStruct)
        end
      end
    end

    private

    def memoize_results(key)
      return instance_variable_get(key) if instance_variable_defined?(key)

      instance_variable_set(key, yield).freeze
    end
  end
end
