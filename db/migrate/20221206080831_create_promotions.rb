# frozen_string_literal: true

class CreatePromotions < ActiveRecord::Migration[6.1]
  def change
    create_table :promotions do |t|
      t.string :name, null: false
      t.integer :movie_showtime_id, null: false
      t.string :item, null: false
      t.integer :discount_percentage, default: 0
    
      t.timestamps
    end
    
    add_foreign_key :promotions, :movie_showtimes, column: :movie_showtime_id
  end
end
