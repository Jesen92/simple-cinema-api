FactoryBot.define do
  factory :promotion do
    name { "Popcorn promotion" }
    item { "popcorn" }
    discount_percentage { 1 }

    association :movie_showtime, factory: :movie_showtime
  end
end
