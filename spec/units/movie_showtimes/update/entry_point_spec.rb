# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MovieShowtimes::Update::EntryPoint do
  subject { described_class.new(params: params, movie_showtime: movie_showtime, promotion: promotion).call }

  let(:movie_showtime) { create(:movie_showtime) }
  let(:movie) { create(:movie) }
  let(:promotion) { nil }
  let(:params) {
    {
      movie_id: movie.id,
      showtime: showtime,
      ticket_price_pennies: ticket_price_pennies
    }
  }

  before do
    freeze_time

    movie_showtime
  end

  context 'when showtime attributes are valid' do
    let(:showtime) { DateTime.now + 3.days }
    let(:ticket_price_pennies) { 16_000 }

    it 'updates the movie showtime' do
      old_movie_showtime = movie_showtime
      expect { subject }.to change(movie_showtime, :movie_id)
        .from(old_movie_showtime.movie_id).to(params[:movie_id])
        .and change(movie_showtime,
                    :showtime)
        .from(old_movie_showtime.showtime)
        .to(params[:showtime])
        .and change(movie_showtime,
                    :ticket_price_pennies)
        .from(old_movie_showtime.ticket_price_pennies)
        .to(params[:ticket_price_pennies])
    end

    context 'when promotion is present' do
      let(:promotion) { Constants.promotions.items.popcorn }

      it 'updates the movie showtime' do
        expect { subject }.to change { Promotion.count }.by(1)
      end
    end

    context 'when promotion does not exist' do
      let(:promotion) { 'non-existant' }

      it 'raises an error' do
        expect { subject }.to raise_error(MovieShowtimes::Update::Action::InvalidStrategy)
      end
    end
  end

  context 'when showtime is in the past' do
    let(:showtime) { DateTime.now - 3.days }
    let(:ticket_price_pennies) { 16_000 }

    it_behaves_like 'raises a ValidationError'
  end
end
