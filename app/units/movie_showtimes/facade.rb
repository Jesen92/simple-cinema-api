# frozen_string_literal: true

module MovieShowtimes
  class Facade
    include ::Core::Utils::Facade

    # MovieShowtimes::Facade.create(params: params) 
    has_use_case :create, Create::EntryPoint
    has_use_case :update, Update::EntryPoint
    has_use_case :destroy, Destroy::EntryPoint
  end
end
