# frozen_string_literal: true

module MovieShowtimes
  module Create
    class Action
      def initialize(form:)
        @form = form
      end

      def call
        # There could be more logic or subactions before the creation itself
        MovieShowtime.create(form.to_h)
      end

      private

      attr_reader :form
    end
  end
end
