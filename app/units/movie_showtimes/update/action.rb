# frozen_string_literal: true

module MovieShowtimes
  module Update
    class Action

      InvalidStrategy = Class.new(StandardError)

      def initialize(form:, movie_showtime:, promotion:)
        @form = form
        @movie_showtime = movie_showtime
        @promotion = promotion
      end

      def call
        movie_showtime.update!(form.to_h)
        strategy_klass.call if promotion.present?
        movie_showtime
      end

      private

      attr_reader :form, :movie_showtime, :promotion

      def strategy_klass
        raise InvalidStrategy unless valid_strategy?

        "#{self.class.module_parent}::Strategy::#{promotion.classify}Promotion"
          .constantize.new(movie_showtime: movie_showtime)
      end

      def valid_strategy?
        Constants.promotions.types.include? promotion
      end
    end
  end
end
