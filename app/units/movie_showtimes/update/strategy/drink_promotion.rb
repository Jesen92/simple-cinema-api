# frozen_string_literal: true

module MovieShowtimes
  module Update
    module Strategy
      class DrinkPromotion

        def initialize(movie_showtime:)
          @movie_showtime = movie_showtime
        end

        def call
          promotion = Promotion.new(promotion_params)
          promotion.save
        end

        private

        attr_reader :movie_showtime

        def promotion_params
          drink_promotion = Constants.promotions.drink_promotion
          {
            movie_showtime_id: movie_showtime.id,
            name: drink_promotion.name,
            item: drink_promotion.item,
            discount_percentage: drink_promotion.discount_percentage
          }
        end
      end
    end
  end
end
