# frozen_string_literal: true

module MovieShowtimes
  module Update
    module Strategy
      class PopcornPromotion

        def initialize(movie_showtime:)
          @movie_showtime = movie_showtime
        end

        def call
          promotion = Promotion.new(promotion_params)
          promotion.save
        end

        private

        attr_reader :movie_showtime

        def promotion_params
          popcorn_promotion = Constants.promotions.popcorn_promotion
          {
            movie_showtime_id: movie_showtime.id,
            name: popcorn_promotion.name,
            item: popcorn_promotion.item,
            discount_percentage: popcorn_promotion.discount_percentage
          }
        end
      end
    end
  end
end
