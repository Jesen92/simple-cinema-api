# frozen_string_literal: true

module MovieShowtimes
  module Update
    class EntryPoint
      def initialize(params:, movie_showtime:, promotion: nil)
        form = Core::Forms::MovieShowtime.new.call(params)
        @action = Action.new(form: form,
                             movie_showtime: movie_showtime,
                             promotion: promotion)
      end

      def call
        action.call
      end

      private

      attr_reader :action
    end
  end
end
