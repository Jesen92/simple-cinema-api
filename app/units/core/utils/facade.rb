# frozen_string_literal: true

module Core
  module Utils
    module Facade
      extend ActiveSupport::Concern

      included do |base|
        base.extend Dry::Container::Mixin
      end

      module ClassMethods
        private

        def has_use_case(name, unit_class)
          register(name) { unit_class }
          define_singleton_method(name) { unit_class }
        end
      end
    end
  end
end

=begin
# Set up a container (using dry-container here)
class MyContainer
  extend Dry::Container::Mixin

  register "users_repository" do
    UsersRepository.new
  end

  register "operations.create_user" do
    CreateUser.new
  end
end

# Set up your auto-injection mixin
Import = Dry::AutoInject(MyContainer)

class CreateUser
  include Import["users_repository"]

  def call(user_attrs)
    users_repository.create(user_attrs)
  end
end

-------------------------
Don't do this :puke
create_user = MyContainer["operations.create_user"]
create_user.call(name: "Jane")
=end
